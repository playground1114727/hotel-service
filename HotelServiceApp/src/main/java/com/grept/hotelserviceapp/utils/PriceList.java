package com.grept.hotelserviceapp.utils;


public abstract class PriceList {
    public static final Double PRICE_AIRCO = 30.45;
    public static final Double PRICE_BALCONY = 10.23;
    public static final Double PRICE_EXTRA_BED = 16.75;
    public static final Double PRICE_BATHTUB = 20.50;
    public static final Double PRICE_BREAKFAST = 16.50;

}
