package com.grept.hotelserviceapp.service.impl;

import com.grept.hotelserviceapp.model.RoomOption;
import com.grept.hotelserviceapp.repository.RoomOptionRepository;
import com.grept.hotelserviceapp.service.RoomOptionService;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class RoomOptionServiceImpl implements RoomOptionService {

    private final RoomOptionRepository repository;

    @Override
    public Optional<RoomOption> getRoomOption(final Long id) {
        return repository.findById(id);
    }

    @Override
    public List<RoomOption> getRoomOptions(final Long id) {
        return repository.findAll();
    }
}
