package com.grept.hotelserviceapp.service.impl;

import com.grept.hotelserviceapp.dataobject.BookingDO;
import com.grept.hotelserviceapp.dataobject.RoomDO;
import com.grept.hotelserviceapp.model.QBooking;
import com.grept.hotelserviceapp.model.User;
import com.grept.hotelserviceapp.repository.BookingRepository;
import com.grept.hotelserviceapp.service.BookingService;
import com.querydsl.core.BooleanBuilder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository repository;

    private final ModelMapper mapper = new ModelMapper();

    @Override
    public BookingDO getBooking(UUID bookingUuid) {
        return mapper.map(repository.findByUuid(bookingUuid), BookingDO.class);
    }

    @Override
    public List<BookingDO> getAllBookings() {
        return repository.findAll().stream()
                .map(obj -> mapper.map(obj, BookingDO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<BookingDO> getBookingsForUser(User user) {
        return null;
    }

    @Override
    public List<BookingDO> getPastBookings() {

        final BooleanBuilder predicate = new BooleanBuilder(
                QBooking.booking.endDate.before(LocalDateTime.now()));

        final List<BookingDO> pastBookings = new ArrayList<>();

        repository.findAll(predicate).forEach(booking -> mapper.map(booking, BookingDO.class));

        return pastBookings;
    }

    @Override
    public BookingDO createNewBooking(final User user,
                                      final LocalDateTime startDate,
                                      final LocalDateTime endDate,
                                      final List<RoomDO> rooms) {


        return null;
    }
}
