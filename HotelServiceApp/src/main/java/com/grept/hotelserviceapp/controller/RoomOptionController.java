package com.grept.hotelserviceapp.controller;

import com.grept.hotelserviceapp.model.RoomOption;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RoomOptionController extends AbstractController {

    public static final String PATH = "/room-option";

    public static final String PAGE_TITLE = "Room Options";

    @GetMapping(PATH)
    public ResponseEntity<RoomOption> getRoomOption() {

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public String addPageTitleToModel() {
        return PAGE_TITLE;
    }
}
