package com.grept.hotelserviceapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController extends AbstractController{

    public final static String PATH = "/home";

    public static final String PAGE_TITLE = "Hotel Service Application";

    @GetMapping(PATH)
    public ModelAndView getHomePage() {
        return new ModelAndView("home");
    }

    @Override
    public String addPageTitleToModel() {
        return PAGE_TITLE;
    }
}
