package com.grept.hotelserviceapp.model;

import org.hibernate.annotations.Where;

@Where(clause = "is_deleted = false")
public interface SoftDeletable {

    boolean isDeleted();
}
