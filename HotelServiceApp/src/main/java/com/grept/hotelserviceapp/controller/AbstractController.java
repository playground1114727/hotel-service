package com.grept.hotelserviceapp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatterBuilder;

import static com.grept.hotelserviceapp.controller.Defaults.*;

@Slf4j
public abstract class AbstractController {

    @Value("${git.commit.id.abbrev}")
    private String commitId;

    @ModelAttribute(PARAM_TIMESTAMP)
    public String getTimeStamp() {
        return LocalDateTime.now()
                .format(new DateTimeFormatterBuilder()
                        .appendPattern("HH:mm:ss")
                        .toFormatter());
    }

    @ModelAttribute(PARAM_GIT_COMMIT_ID)
    public String getCommitId() {
        return commitId;
    }


    @ModelAttribute(PARAM_PAGE_TITLE)
    abstract public String addPageTitleToModel();


}
