package com.grept.hotelserviceapp.utils;


import com.grept.hotelserviceapp.dataobject.BookingDO;
import com.grept.hotelserviceapp.dataobject.RoomDO;
import com.grept.hotelserviceapp.model.Room;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.awt.print.Book;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Data
@Accessors(fluent = true)
@NoArgsConstructor
public class BookingDOBuilder {

    private Long id = 1L;

    // required
    private UUID uuid;

    // optional
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private List<RoomDO> rooms = Collections.emptyList();
    private boolean isNew;

    public BookingDO build() {
        final BookingDO bookingDO = getInstance();

        bookingDO.setUuid(uuid);
        bookingDO.setStartDate(startDate);
        bookingDO.setEndDate(endDate);
        bookingDO.setRooms(rooms);

        return bookingDO;
    }

    protected BookingDO getInstance() {
        return new BookingDO();
    }

}
