package com.grept.hotelserviceapp.service;

import com.grept.hotelserviceapp.dataobject.BookingDO;
import com.grept.hotelserviceapp.dataobject.RoomDO;
import com.grept.hotelserviceapp.model.Booking;
import com.grept.hotelserviceapp.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public interface BookingService {

    BookingDO getBooking(final UUID bookingUuid);

    List<BookingDO> getAllBookings();


    // FIXME: Use UserDo or something like CurrentSecurityContext to identify user.
    List<BookingDO> getBookingsForUser(final User user);

    List<BookingDO> getPastBookings();

    BookingDO createNewBooking(final User user, final LocalDateTime startDate, final LocalDateTime endDate, final List<RoomDO> rooms);
}
