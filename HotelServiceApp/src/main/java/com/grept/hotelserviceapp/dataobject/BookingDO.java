package com.grept.hotelserviceapp.dataobject;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class BookingDO {

    private UUID uuid;

    private LocalDateTime bookingDate;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private List<RoomDO> rooms;
}
