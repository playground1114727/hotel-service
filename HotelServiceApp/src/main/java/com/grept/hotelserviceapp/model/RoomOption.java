package com.grept.hotelserviceapp.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Entity
@RequiredArgsConstructor
public class RoomOption extends Option {

    @ManyToMany
    private List<Room> rooms;
}
