package com.grept.hotelserviceapp.repository;

import com.grept.hotelserviceapp.model.RoomOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomOptionRepository extends JpaRepository<RoomOption, Long> {
}
