package com.grept.hotelserviceapp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.AbstractAuditable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Booking extends AbstractAuditable<User, Long> implements SoftDeletable {

    @NotNull
    private UUID uuid;

    @NotNull
    @Column(name = "booking_date")
    private LocalDateTime bookingDate = LocalDateTime.now();

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @OneToMany(mappedBy = "bookings", fetch = FetchType.LAZY)
    private List<Room> rooms;

    @ManyToOne
    @JoinTable(name = "hotel_booking")
    private Hotel hotel;

    private boolean deleted;

    @Override
    public boolean isNew() {
        return getId() == null;
    }
}
