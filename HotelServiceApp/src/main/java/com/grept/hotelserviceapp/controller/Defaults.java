package com.grept.hotelserviceapp.controller;

public class Defaults {

    public final static String PARAM_TIMESTAMP = "timestamp";
    public final static String PARAM_PAGE_TITLE = "page_title";
    public final static String PARAM_GIT_COMMIT_ID = "git_commit_id";
}
