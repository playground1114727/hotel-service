package com.grept.hotelserviceapp.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class RoomController extends AbstractController {

    public static final String PATH = "/room";

    public static final String PAGE_TITLE = "Rooms";

    @GetMapping(PATH)
    public ModelAndView getRoomPage(HttpServletRequest request) {
        return new ModelAndView("room");
    }

    @Override
    public String addPageTitleToModel() {
        return PAGE_TITLE;
    }
}
