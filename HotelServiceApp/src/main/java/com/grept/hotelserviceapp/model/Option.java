package com.grept.hotelserviceapp.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@RequiredArgsConstructor
public class Option {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String discription;
    private Double price;
}
