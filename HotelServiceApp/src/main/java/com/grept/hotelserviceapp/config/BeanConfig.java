package com.grept.hotelserviceapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class BeanConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propertiesConfig = new PropertySourcesPlaceholderConfigurer();
        propertiesConfig.setLocation(new ClassPathResource("application-git.properties"));
        propertiesConfig.setIgnoreResourceNotFound(true);
        propertiesConfig.setIgnoreUnresolvablePlaceholders(true);

        return propertiesConfig;
    }
}
