package com.grept.hotelserviceapp.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
public class DateRange {

    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
}
