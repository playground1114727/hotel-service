package com.grept.hotelserviceapp.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    @Id
    @GeneratedValue
    private Long id;

    private String roomNumber;

    private String description;

    private RoomType size;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "booking_room")
    private Booking bookings;

    @NotNull
    private boolean isVacant = true;

    private Integer bedCount;

    @ManyToOne
    @JoinTable(name = "hotel_room")
    private Hotel hotel;

    @ManyToMany
    private List<RoomOption> roomOptions = new ArrayList<>();

}
