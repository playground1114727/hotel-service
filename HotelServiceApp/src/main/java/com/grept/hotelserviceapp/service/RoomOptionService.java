package com.grept.hotelserviceapp.service;

import com.grept.hotelserviceapp.model.RoomOption;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface RoomOptionService {

    Optional<RoomOption> getRoomOption(final Long id);

    List<RoomOption> getRoomOptions(final Long id);
}
