package com.grept.hotelserviceapp.controller;

import com.grept.hotelserviceapp.dataobject.BookingDO;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.info.GitProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class BookingController extends AbstractController{

    public static final String PATH = "/booking";
    public static final String PAGE_TITLE = "Bookings";

    @GetMapping(PATH)
    public ModelAndView getBookingPage(final HttpServletRequest request) {

        return new ModelAndView("bookings");
    }

    @Override
    public String addPageTitleToModel() {
        return PAGE_TITLE;
    }
}
