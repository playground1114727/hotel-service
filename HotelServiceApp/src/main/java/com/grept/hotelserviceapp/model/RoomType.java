package com.grept.hotelserviceapp.model;

public enum RoomType {

    SMALL,
    MEDIUM,
    LARGE,
    BRIDAL;

    RoomType() {
    }
}
